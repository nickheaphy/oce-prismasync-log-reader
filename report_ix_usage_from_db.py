# Report on the data from the database
# This just prints the data to screen

import inc.DB_Class as DB_Class
import logging
import inc.DB_Reporting_iX_click as DB_Reporting_iX_click
import datetime
import config

def floatconv(value):
    try:
        float(value)
        return float(value)
    except ValueError:
        return 0.0


def main():

    with DB_Reporting_iX_click.IXReporting(
        config.config["databasename"],
        config.config["machinename"]
    ) as database:

        # print("\nInk Consumption Total")
        # print("-----------------------")
        # inkml, cgml, totalA4, duration = database.return4totalinkusage("2021-12-16 00:00:00")
        # print(f"{totalA4} A4 - {inkml:.1f}ml,  {cgml:.1f}ml")
        # print(f"{totalA4} A4 - {inkml/1000/4.7:.1f} bottles,  {cgml/1000/4.1:.1f} bottles")
        # print(f"Ratio: {(cgml/1000/4.1)/(inkml/1000/4.7):.1f} cg bottle to every ink bottle")

        print(database)

        


if __name__ == "__main__":

    # create logger
    logger = logging.getLogger('oce_accounting')
    logger.setLevel(config.config["loglevel"])
    # create file handler which logs even debug messages
    fh = logging.FileHandler('oce_accounting.log')
    fh.setLevel(config.config["loglevel"])
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(config.config["loglevel"])
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    #ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    main()