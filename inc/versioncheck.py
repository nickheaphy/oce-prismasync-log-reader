# Tools to check the version online

__home_url__ = "https://bitbucket.org/nickheaphy/oce-prismasync-log-reader"

# ---------------------------------------------------------------
def return_latest_online_version():
    '''Get the latest version number from repository and return'''
    try:
        import urllib.request
        response = urllib.request.urlopen(__home_url__+"/raw/HEAD/README.md")
        data = response.read()
        text = data.decode('utf-8')
        text = text.splitlines()
        for line in text:
            if line[:2] == "v ":
                try:
                    return float(line.split(" ")[1])
                except:
                    return None
    except:
        pass
    return None