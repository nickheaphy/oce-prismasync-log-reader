# This is the generic reporting class for the PrismaSync data
# This inherits from the DB class

import sqlite3
from datetime import datetime
import logging
import sys
import inc.DB_Class as DB_Class

class Reporting(DB_Class.DB):

    def __init__(self, the_database):

        if not hasattr(self, 'machinename'):
            self.machinename = ""
        super().__init__(the_database)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)

    def divideby(self,num1,num2,inthecaseoferror='Undefined'):
        '''Convert num1 and num2 to floats and attempt to divide them.
        If conversion or division fails, return incaseoferror'''
        try:
            num1 = float(num1)
            num2 = float(num2)
            result = num1/num2
            return result
        except:
            return inthecaseoferror

    # def returndatabaseperiod(self,tablename):
    #     '''Figure out the first and last dates in the database'''
    
    def return2databaseperiod(self):
        '''Figure out the first and last dates in the database
        returns tuple firstdate, lastdate'''
        theSQL = "SELECT MAX(date(readydate)) AS lastdate, MIN(date(readydate)) AS firstdate FROM {}_all;".format(self.machinename)
        result = self.selectSQLandReturnOneRow(theSQL)
        return result['firstdate'], result['lastdate']