# Build/update the SQLite database

import sqlite3
from datetime import datetime
import logging
import sys
import inc.parselogfile as parselogfile
import os

class DB:

    current_db_version = 0

    def __init__(self, the_database):
        # os.chdir(os.path.dirname(os.path.realpath(__file__)))
        scriptpath = os.path.dirname(os.path.realpath(__file__))
        self.conn = sqlite3.connect(os.path.join(scriptpath, "..", the_database))
        self.conn.row_factory = sqlite3.Row
        self.logger = logging.getLogger('oce_accounting')
        self.logger.debug("Creating/Opening DB instance")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self.conn.commit()
        except Exception as e:
            print("Database Error: Rolling Back")
            self.logger.error("DB Error: {}".format(e))
            self.conn.rollback()
        finally:
            self.logger.debug("DB Closing.")
            self.conn.close()
    
    # ---------------------------------------------------------------
    def getLogs(self,baseurl,machinename):
        acl = parselogfile.returnaclfilename(baseurl)
        if acl is not None:
            columns = parselogfile.returnColumnNames(baseurl+"/"+acl)
            self._setuptables(machinename,columns)

            # # now throw some data in...
            csvlist = parselogfile.returncsvfilenames(baseurl)
            for csv in csvlist:
                # have we processed this before?
                if not self._alreadyprocessed(machinename,csv):
                    data = parselogfile.returnLogFileData(baseurl+"/"+csv)
                    if data is not None:
                        for line in data:
                            self._feedrecord(machinename,columns,line)
            # now process the acl file
            data = parselogfile.returnLogFileData(baseurl+"/"+acl)
            if data is not None:
                for line in data:
                    self._feedrecord(machinename+"_acl",columns,line)
            
            self.commitdata()
        else:
            self.logger.error("Did not get a valid .ACL file from the PrismaSync.")


    # ---------------------------------------------------------------
    def _setuptables(self, machinename, column_data):

        cur = self.conn.cursor()

        # This is so next time I can just update the database structure
        # when I want to add something...
        try:
            cur.execute('''SELECT version FROM db_version WHERE id=1''')
            db_version = (cur.fetchone())[0]
        except:
            db_version = 0

        if db_version == 0:
            
            #create a `machine` and `machine_acl` table
            self._generateTablesDirect(machinename, column_data)

            # create view combining `machine` and `machine_acl` table
            cur.execute('''CREATE VIEW IF NOT EXISTS {}_all AS SELECT * FROM {} UNION SELECT * FROM {}_acl;'''.format(machinename,machinename,machinename))
            
            # create csv table to hold what has been processed already
            cur.execute('''CREATE TABLE IF NOT EXISTS {}_csv (
                id INTEGER PRIMARY KEY, csv_file TEXT);'''.format(machinename))

            # create the table to store the database version
            cur.execute('''CREATE TABLE IF NOT EXISTS db_version (
                id INTEGER PRIMARY KEY, version INT);''')
        
        if db_version < self.current_db_version:
            # Perform updates if needed
            if db_version == 1:
                # Perform updates required to bring database from v1 to current version
                pass

        # Set the correct current version in the database
        cur.execute('''REPLACE INTO db_version (id, version) VALUES (%s, %s)''' % (1, self.current_db_version))

        self.commitdata()

    # ---------------------------------------------------------------
    def execSQLandReturnLastRowID(self, sql):
        '''Execute a SQL query and return the last row ID'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql)
            return cur.lastrowid
        except Exception as e:
            self.logger.error("SQL Error: {} using {}".format(e,sql))
            return None

    # ---------------------------------------------------------------
    def selectSQLandReturnOneRow(self, sql):
        '''Execute a SQL query and return one row'''
        cur = self.conn.cursor()
        try:
            cur.execute(sql)
            return cur.fetchone()
        except Exception as e:
            self.logger.error("SQL Error: {} using {}".format(e,sql))
            return None

    # ---------------------------------------------------------------
    def selectSQLandReturnResult(self, sql):
        '''Execute a SQL query and return results '''
        cur = self.conn.cursor()
        try:
            cur.execute(sql)
            return cur.fetchall()
        except Exception as e:
            self.logger.error("SQL Error: {} using {}".format(e,sql))
            return None

    # ---------------------------------------------------------------
    def selectSQLandReturnResultandCursor(self, sql):
        '''Execute a SQL query and return results '''
        cur = self.conn.cursor()
        try:
            cur.execute(sql)
            return cur.fetchall(), cur
        except Exception as e:
            self.logger.error("SQL Error: {} using {}".format(e,sql))
            return None, None

    # ---------------------------------------------------------------
    def _feedrecord(self, tablename, columns, data):
        '''Receive a row of data to insert into the database'''
        #check if we need to split
        if isinstance(data, str):
            data = data.split(";")
        if isinstance(columns, str):
            columns = columns.split(";")
        
        # check that the data and columns are the same
        if len(data) != len(columns):
            self.logger.error("There are a different number of data columns and database columns, skipping")
            self.logger.debug("Columns: {}".format(columns))
            self.logger.debug("Data: {}".format(data))
            return None
        
        # TODO - Rewrite this to use parameters (to avoid the problem with special characters)
        theSQL = "INSERT INTO {} (".format(tablename)
        for col in columns:
            theSQL += "`{}`,".format(col)
        theSQL = theSQL[:-1] + ") VALUES ("
        for dat in data:
            # need to format the data to remove '
            theSQL += "'{}',".format(dat.replace("'",""))
        theSQL = theSQL[:-1] + ");"

        try:
            self.conn.cursor().execute(theSQL)
        except sqlite3.IntegrityError:
            # This could happen if we happen to reprocess a CSV
            # shouldnt happen, but better to catch than crash...
            pass
        except Exception as e:
            self.logger.error("SQL Error: {} using {}".format(e,theSQL))

    # ---------------------------------------------------------------
    def commitdata(self):
        self.logger.info("Commiting Data")
        try:
            self.conn.commit()
        except sqlite3.Error:
            print("Commit failed! Rolling back...")
            self.conn.rollback()
    
    # ---------------------------------------------------------------
    def _alreadyprocessed(self,machinename,csv):
        '''Check if we have already processed this
           If we have, return true.
           If we have not, store in the database and return false.
           Ignores .ACL files (already return false)'''
        if csv.upper().endswith(".ACL"):
            self.logger.debug("Always process {}".format(csv))
            return False
        
        theSQL = "SELECT csv_file FROM {}_csv".format(machinename)
        try:
            csv_allreadyprocessed = self.conn.cursor().execute(theSQL).fetchall()
        except Exception:
            self.logger.error("SQL Error: {} using {}".format(sys.exc_info()[0],theSQL))
        
        for filename in csv_allreadyprocessed:
            if filename["csv_file"] == csv:
                self.logger.debug("Processed already, skipping {}".format(csv))
                return True
        
        #we have not processed already
        self.logger.debug("Processing {}".format(csv))
        theSQL = "INSERT INTO {}_csv (csv_file) VALUES ('{}');".format(machinename,csv)
        try:
            self.conn.cursor().execute(theSQL)
        except Exception:
            self.logger.error("SQL Error: {} using {}".format(sys.exc_info()[0],theSQL))
        return False

    # ---------------------------------------------------------------
    def _generateTablesDirect(self, machinename, newcols):
        '''Generate the accounting database (or update the database with new columns)'''
        if isinstance(newcols, str):
            #need to split the string
            newcols = newcols.split(";")
        
        cur = self.conn.cursor()
        originalcols = self._returnCurrentColumns(machinename)

        if len(originalcols) > 0:
            # see if there are any differences between the old table and the new table
            additionalcols = set(newcols) - set(originalcols)

            if len(additionalcols) > 0:
                self.logger.warn("Column data has changed. Need to adjust database")
                # need to create a new table with the old table columns plus the new ones
                theSQL = "CREATE TABLE IF NOT EXISTS `{}_temp` (".format(machinename)
                for col in originalcols + list(additionalcols):
                    theSQL += "`{}` TEXT,".format(col)
                theSQL += " UNIQUE ("
                for col in originalcols + list(additionalcols):
                    theSQL += "`{}`,".format(col)
                theSQL = theSQL[:-1] + "));"

                try:
                    cur.execute(theSQL)
                except Exception as e:
                    self.logger.error("SQL Error (creating _temp table): {} using {}".format(e,theSQL))

                # now copy the old data over
                theSQL = "INSERT INTO {}_temp (".format(machinename)
                for col in originalcols:
                    theSQL += "`{}`,".format(col)
                theSQL = theSQL[:-1] + ") SELECT "
                for col in originalcols:
                    theSQL += "`{}`,".format(col)
                theSQL = theSQL[:-1] + " FROM {}".format(machinename)

                try:
                    cur.execute(theSQL)
                except Exception as e:
                    self.logger.error("SQL Error (copying to _temp table): {} using {}".format(e,theSQL))

                # now drop the old table
                theSQL = "DROP TABLE IF EXISTS {};".format(machinename)
                try:
                    cur.execute(theSQL)
                except Exception as e:
                    self.logger.error("SQL Error (dropping table): {} using {}".format(e,theSQL))

                # now rename the temp table
                theSQL = "ALTER TABLE {}_temp RENAME TO {};".format(machinename,machinename)
                try:
                    cur.execute(theSQL)
                except Exception as e:
                    self.logger.error("SQL Error (renaming table): {} using {}".format(e,theSQL))


        # because we want this to be as generic as possible, we use text
        # have a unique constraint over the whole row in case I try and add the
        # same data twice (when processing the ACL file)
        theSQL = "CREATE TABLE IF NOT EXISTS `{}` (".format(machinename)
        for col in newcols:
            theSQL += "`{}` TEXT,".format(col)
        theSQL += " UNIQUE ("
        for col in newcols:
            theSQL += "`{}`,".format(col)
        theSQL = theSQL[:-1] + "));"
        
        try:
            cur.execute(theSQL)
        except Exception as e:
            self.logger.error("SQL Error (creating table): {} using {}".format(e,theSQL))

        # have decided to separate the acl data into its own table. This is because have see
        # occurrences of weird data in the ACL file for jobs that have only just completed or
        # and in the process of being finished. Don't want this weird data getting into the main
        # table. Going to take the performance hit of always processing the ACL.
        theSQL = "DROP TABLE IF EXISTS `{}_acl`;".format(machinename)
        try:
            cur.execute(theSQL)
        except Exception as e:
            self.logger.error("SQL Error (dropping _acl): {} using {}".format(e,theSQL))

        # don't need the unique constraint as this table is rebuilt every time
        theSQL = "CREATE TABLE `{}_acl` (".format(machinename)
        for col in newcols:
            theSQL += "`{}` TEXT,".format(col)
        theSQL = theSQL[:-1] + ");"

        try:
            cur.execute(theSQL)
        except Exception as e:
            self.logger.error("SQL Error (creating _acl table): {} using {}".format(e,theSQL))

        self.commitdata()

    # ---------------------------------------------------------------
    def _returnCurrentColumns(self, machinename):
        '''Return a list of the column names in the tablename'''
        cur = self.conn.cursor()
        theSQL = '''PRAGMA table_info('{}')'''.format(machinename)
        currentcols = cur.execute(theSQL).fetchall()
        columnnames = []
        for c in currentcols:
            columnnames.append(c['name'])
        return columnnames

    