# Reporting for the Colorado 1640 Only
# This inherits from the generic reporting class

import inc.DB_Reporting_Class as DB_Reporting_Class
import datetime
import re

class ColoradoReporting(DB_Reporting_Class.Reporting):

    def __init__(self, the_database, machinename, inkprice):
        '''inkprice is a tuple of the four CMYK prices)'''
        # Init the generic reporting
        self.machinename = machinename
        # Ink pricing
        self.c_p = inkprice[0]
        self.m_p = inkprice[1]
        self.y_p = inkprice[2]
        self.k_p = inkprice[3]
        super().__init__(the_database)

    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)

    def return2databaseperiod(self):
        '''Figure out the first and last dates in the database
        returns tuple firstdate, lastdate'''
        theSQL = "SELECT MAX(date(readydate)) AS lastdate, MIN(date(readydate)) AS firstdate FROM {}_all;".format(self.machinename)
        result = self.selectSQLandReturnOneRow(theSQL)
        return result['firstdate'], result['lastdate']

    
    def return4totalinkusage(self):
        '''Returns sqm, inkml, totalduration, spend totals'''
        theSQL = "SELECT SUM(printedarea) as printedarea, \
        SUM((CAST(inkcolorcyan AS INTEGER) + \
           CAST(inkcoloryellow AS INTEGER) + \
           CAST(inkcolormagenta AS INTEGER) + \
           CAST(inkcolorblack AS INTEGER))) \
           as ink, \
        SUM((CAST(inkcolorcyan AS FLOAT)/1000*{} + \
           CAST(inkcoloryellow AS FLOAT)/1000*{} + \
           CAST(inkcolormagenta AS FLOAT)/1000*{} + \
           CAST(inkcolorblack AS FLOAT)/1000*{})) \
           as inkprice, \
           SUM(strftime('%s', (CASE length(activetime) WHEN 7 THEN '0' || activetime ELSE activetime END)) - strftime('%s', '00:00:00')) as duration_sec \
           FROM {}_all".format(self.c_p,self.m_p,self.y_p,self.k_p,self.machinename)
        
        result = self.selectSQLandReturnOneRow(theSQL)
        if result is not None:
            sqm = self.divideby(result["printedarea"],1000000)
            inkml = self.divideby(result["ink"],1000)
            totalduration = self.divideby(self.divideby(result["duration_sec"],60),60)

            return sqm, inkml, totalduration, result["inkprice"]
        else:
            return None, None, None, None

    def returninkconsumptionbymedia(self):
        theSQL = "SELECT mediatype, SUM(CAST(printedarea AS INTEGER)) as area, \
		   SUM((CAST(inkcolorcyan AS INTEGER) + \
           CAST(inkcoloryellow AS INTEGER) + \
           CAST(inkcolormagenta AS INTEGER) + \
           CAST(inkcolorblack AS INTEGER))) \
           as ink, \
           SUM((CAST(inkcolorcyan AS FLOAT)/1000*{} + \
           CAST(inkcoloryellow AS FLOAT)/1000*{} + \
           CAST(inkcolormagenta AS FLOAT)/1000*{} + \
           CAST(inkcolorblack AS FLOAT)/1000*{})) \
           as inkprice \
           from {}_all GROUP BY mediatype".format(self.c_p,self.m_p,self.y_p,self.k_p,self.machinename)
        
        result = self.selectSQLandReturnResult(theSQL)
        return result

    def returninkconsumptionbymode(self):
        theSQL = "SELECT printmode, SUM(CAST(printedarea AS INTEGER)) as area, \
		   SUM((CAST(inkcolorcyan AS INTEGER) + \
           CAST(inkcoloryellow AS INTEGER) + \
           CAST(inkcolormagenta AS INTEGER) + \
           CAST(inkcolorblack AS INTEGER))) \
           as ink, \
           SUM((CAST(inkcolorcyan AS FLOAT)/1000*{} + \
           CAST(inkcoloryellow AS FLOAT)/1000*{} + \
           CAST(inkcolormagenta AS FLOAT)/1000*{} + \
           CAST(inkcolorblack AS FLOAT)/1000*{})) \
           as inkprice \
           from {}_all GROUP BY printmode".format(self.c_p,self.m_p,self.y_p,self.k_p,self.machinename)
        
        result = self.selectSQLandReturnResult(theSQL)
        return result

    def returnjobsbetweendates(self,begin,end):
        '''Takes SQL formated begin and end dates and gives back the job data.
        Date formated yyyy-mm-dd hh:mm:ss'''
        theSQL = "SELECT strftime('%Y-%m-%d %H:%M:%S', readydate || ' ' || readytime) AS d, \
            printedarea AS area, \
            mediatype, \
            printmode, \
            result, \
		    (CAST(inkcolorcyan AS INTEGER) + CAST(inkcoloryellow AS INTEGER) + \
            CAST(inkcolormagenta AS INTEGER) + CAST(inkcolorblack AS INTEGER)) as ink, \
            (CAST(inkcolorcyan AS FLOAT)/1000*{} + \
            CAST(inkcoloryellow AS FLOAT)/1000*{} + \
            CAST(inkcolormagenta AS FLOAT)/1000*{} + \
            CAST(inkcolorblack AS FLOAT)/1000*{}) \
            as inkprice, \
            jobname \
            FROM {}_all \
            WHERE d >= datetime('{}') AND d <= datetime('{}') \
            ORDER BY d".format(self.c_p,self.m_p,self.y_p,self.k_p,self.machinename,begin,end)
        
        result = self.selectSQLandReturnResult(theSQL)
        self.logger.debug(result)
        return result

    def returnjobstoday(self):
        today = datetime.datetime.today().strftime('%Y-%m-%d')
        return self.returnjobsbetweendates(today+" 00:00:00",today+"23:59:59")

    def returnjobsyesterday(self):
        today = (datetime.datetime.today()-datetime.timedelta(1)).strftime('%Y-%m-%d')
        return self.returnjobsbetweendates(today+" 00:00:00",today+"23:59:59")
    
    def returnlastxjobs(self,numjobs):
        '''Returns the last numjobs jobs'''
        theSQL = "SELECT strftime('%Y-%m-%d %H:%M:%S', readydate || ' ' || readytime) AS d, \
            printedarea AS area, \
            mediatype, \
            printmode, \
            jobid, \
            documentid, \
            result, \
		    (CAST(inkcolorcyan AS INTEGER) + CAST(inkcoloryellow AS INTEGER) + \
            CAST(inkcolormagenta AS INTEGER) + CAST(inkcolorblack AS INTEGER)) as ink, \
            (CAST(inkcolorcyan AS FLOAT)/1000*{} + \
            CAST(inkcoloryellow AS FLOAT)/1000*{} + \
            CAST(inkcolormagenta AS FLOAT)/1000*{} + \
            CAST(inkcolorblack AS FLOAT)/1000*{}) \
            as inkprice, \
            jobname \
            FROM {}_all \
            ORDER BY d DESC \
            LIMIT {}".format(self.c_p,self.m_p,self.y_p,self.k_p,self.machinename,numjobs)
        
        result = self.selectSQLandReturnResult(theSQL)
        return result

    def returnjobdetails(self,jobid,documentid):
        '''return row(s) about a specific jobid and documentid'''
        # Better use Parameterized queries here as this is user submitted data
        jobid = re.sub('[^0-9]','', jobid)
        theSQL = "SELECT \
        (CAST(inkcolorcyan AS FLOAT)/1000*{} + \
            CAST(inkcoloryellow AS FLOAT)/1000*{} + \
            CAST(inkcolormagenta AS FLOAT)/1000*{} + \
            CAST(inkcolorblack AS FLOAT)/1000*{}) \
            as inkprice, * FROM {}_all WHERE CAST(jobid AS INTEGER) = ? AND documentid = ?".format(self.c_p,self.m_p,self.y_p,self.k_p,self.machinename)
        result = self.conn.execute(theSQL,(jobid,documentid))
        return result