# Reporting for the iX Only using the click model
# This inherits from the generic reporting class

import inc.DB_Reporting_Class as DB_Reporting_Class
import datetime
import re

class IXReporting(DB_Reporting_Class.Reporting):


    volume_c = 4.7
    volume_m = 4.7
    volume_y = 4.7
    volume_k = 4.7
    volume_cg = 4.1

    def __init__(self, the_database, machinename, clickprices = None):
        '''inkprice is a tuple of the four CMYK prices)'''
        # Init the generic reporting
        self.machinename = machinename
        # Click pricing
        self.clickprices = clickprices
        super().__init__(the_database)

    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)


    def return4totalinkusage(self, startdate = None, enddate = None):
        '''Returns sqm, inkml, totalduration'''
        theSQL = f"SELECT SUM(nofprinteda4bw + nofprinteda4c + (2*nofprinteda3bw) + (2*nofprinteda4c)) as totalA4, \
        SUM((CAST(inkcolorcyan AS INTEGER) + \
           CAST(inkcoloryellow AS INTEGER) + \
           CAST(inkcolormagenta AS INTEGER) + \
           CAST(inkblack AS INTEGER) + \
           CAST(inkcolorblack AS INTEGER))) \
           as ink, \
        SUM((CAST(colorgrip AS INTEGER))) as colorgrip, \
           SUM(strftime('%s', (CASE length(activetime) WHEN 7 THEN '0' || activetime ELSE activetime END)) - strftime('%s', '00:00:00')) as duration_sec \
           FROM {self.machinename}_all"

        if startdate is not None:
            if enddate is not None:
                theSQL += f" WHERE startdate > DATE('{startdate}' AND startdate <= DATE('{enddate}')"
            else:
                theSQL += f" WHERE startdate > DATE('{startdate}')"
        
        result = self.selectSQLandReturnOneRow(theSQL)

        if result is not None:
            #sqm = self.divideby(result["printedarea"],1000000)
            inkml = self.divideby(result["ink"],1000)
            cgml = self.divideby(result["colorgrip"],1000)
            a4 = result["totalA4"]
            totalduration = self.divideby(self.divideby(result["duration_sec"],60),60)

            return inkml, cgml, a4, totalduration
        else:
            return None, None, None, None

    def __repr__(self) -> str:
        theSQL = f"SELECT \
            jobname, \
            startdate, \
            (nofprinteda4bw + nofprinteda4c + (2*nofprinteda3bw) + (2*nofprinteda3c)) as totalA4, \
            nofprinteda4bw, \
            nofprinteda4c, \
            nofprinteda3bw, \
            nofprinteda3c, \
            nofprinteda4low, \
            nofprinteda3low, \
            nofprinteda4med, \
            nofprinteda3med, \
            nofprinteda4high, \
            nofprinteda3high, \
            inkblack, \
            inkcolorcyan, \
            inkcolormagenta, \
            inkcoloryellow, \
            inkcolorblack, \
            (CAST(inkcolorcyan AS INTEGER) + \
             CAST(inkcoloryellow AS INTEGER) + \
             CAST(inkcolormagenta AS INTEGER) + \
             CAST(inkblack AS INTEGER) + \
             CAST(inkcolorblack AS INTEGER)) \
            as totalink_ul, \
            colorgrip as colorgrip_ul, \
            (CAST( \
             (CAST(inkcolorcyan AS INTEGER) + \
             CAST(inkcoloryellow AS INTEGER) + \
             CAST(inkcolormagenta AS INTEGER) + \
             CAST(inkblack AS INTEGER) + \
             CAST(inkcolorblack AS INTEGER)) AS FLOAT) / 1000 / 1000 / 4.7) \
            as totalink_bottle, \
            (CAST(colorgrip AS FLOAT) / 1000 / 1000 / 4.1) as colorgrip_bottle, \
            ROUND( \
             (CAST(colorgrip AS FLOAT) / 1000 / 1000 / 4.1) / \
             (CAST((CAST(inkcolorcyan AS FLOAT) + \
             CAST(inkcoloryellow AS FLOAT) + \
             CAST(inkcolormagenta AS FLOAT) + \
             CAST(inkblack AS FLOAT) + \
             CAST(inkcolorblack AS FLOAT) \
             ) AS FLOAT)/1000/1000/4.7),3) as bottle_ratio, \
            medianame1, \
            medianame2, \
            medianame3, \
            medianame4 \
        FROM {self.machinename}_all WHERE jobtype == 'IP' ORDER BY startdate"
        result, cursor = self.selectSQLandReturnResultandCursor(theSQL)
        colnames = [desc[0] for desc in cursor.description]
        
        returndata = ""

        for colname in colnames:
            returndata += colname + ","

        returndata += "\r\n"

        for row in result:
            classification = "coated"
            for colname in colnames:
                if "media" in str(colname):
                    if "Advance" in row[colname]:
                        classification = "uncoated"
                if "\"" in str(colname):
                    returndata += "\"" + str(row[colname]) + "\","
                else:
                    returndata += str(row[colname]) + ","

            returndata += classification + "\r\n"

        
        return returndata