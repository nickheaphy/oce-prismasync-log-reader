# Helper functions to get the CSV/ACL files from the Oce PrismaSync controller

import urllib.request
import logging
import re

# create logger
logger = logging.getLogger('oce_accounting')

# ---------------------------------------------------------------
def returnLogFile(url):
    '''Open a CSV/ACL file from the url'''
    try:
        raw = urllib.request.urlopen(url,timeout=5).read()
        # data is UTF encoded with a BOM
        return raw.decode("utf-8-sig")
    except urllib.error.URLError as e:
        logger.error("URLError accessing {}: {}".format(url,e))

    
# ---------------------------------------------------------------
def returnLogFileData(url):
    '''Just return the data part'''
    data = returnLogFile(url)
    if data is not None:
        lines = data.splitlines()
        if len(lines) > 1:
            return lines[1:]


# ---------------------------------------------------------------
def returnColumnNames(url):
    '''Just return the first line of the CSV as a list'''
    data = returnLogFile(url)
    if data is not None:
        return data.splitlines()[0].split(";")


# ---------------------------------------------------------------
def returnlistofcsvandacl(url):
    '''Open the URL and return an array of CSV and ACL files'''
    try:
        raw = urllib.request.urlopen(url,timeout=5).read()
    except urllib.error.URLError as e:
        logger.error("URLError accessing {}: {}".format(url,e))
        return None
    
    stripped_list = re.sub("<.*?>", " ", raw.decode("utf-8-sig")).split(" ")
    return [x for x in stripped_list if not x == '' and x.upper().find(".")!=-1]


# ---------------------------------------------------------------
def returnaclfilename(url):
    '''return just the acl file (return None if not found)'''
    allitems = returnlistofcsvandacl(url)
    if allitems is not None:
        for item in allitems:
            if item.upper().find(".ACL") != -1:
                return item

# ---------------------------------------------------------------
def returncsvfilenames(url):
    '''return just the csv files (return None if no csv files found)'''
    allitems = returnlistofcsvandacl(url)
    csvlist = []
    for item in allitems:
        if item.upper().find(".CSV") != -1:
            csvlist.append(item)
    return csvlist