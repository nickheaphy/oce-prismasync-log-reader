# Python Tool for reading Oce PrismaSync Accounting Log Files

Reads all the Accounting data from an Oce PrismaSync controller and stores it into a SQLite database.

Provides a web based reporting interface to the data (currently only implemented for the Oce Colorado 1640)

### Colorado Reporting

Currently the Colorado report shows Overall Consumption Details allowing you to see the average running costs and speeds of the machine.

There is also detailed usage information on:

* Ink and Usage by Media
* Ink and Usage by Print Mode
* Job details for the last 20 jobs

![Reporting Screenshot](/img/reporting.png)

## Installation

As the only PrismaSync that has currently been tested is the Oce Colorado 1640 the installation instructions are specific to this.

Sorry, no `setup.exe`, you have to do everything by hand :-)

### Windows (on the Onyx Server)

Download and install the latest Python 3 from https://www.python.org/downloads/windows/ (select option to add Python to PATH when installing)

Download the latest code [repository](https://bitbucket.org/nickheaphy/oce-prismasync-log-reader/get/master.zip)

Extract the code repository ZIP file to a folder (eg C:\OceLogReader)

Download NSSM from http://nssm.cc and copy the `nssm.exe` into `C:\OceLogReader`

Open an administrator Command Prompt (`cmd.exe`) (if you don't open as an Administrator you won't be able to use NSSM to install the service)

Install the dependencies for this project

```
pip install Flask
```

### Configuration and Setup

Edit the file `config.py` and specify the appropriate URL for the Colorado (you can ajust any other settings if needed)

You can test the installation by running the following two commands:

```
python C:\OceLogReader\get_accounting_logs_to_db.py
python C:\OceLogReader\report_colorado_usage_from_db.py
```

which should connect to the PrismaSync controller and generate a report to the command line.

To test the webserver functionality you can run

```
python C:\OceLogReader\reporting_webserver.py
```

This will start the Flask webserver and you can access the reporting web page using http://localhost:8080

The default port that the webserver runs on is 8080, however this can be changed in the `config.py` file.

To run the Python webserver as a Windows service you can register this using NSSM.

Run NSSM (from an Administrator command prompt)

```
C:\OceLogReader\nssm.exe install OceReportingServer
```

The Path in NSSM should be the path to the `Python.exe` file (something like `C:\Users\root\AppData\Local\Programs\Python\Python37-32\python.exe`) and the Arguments should be the path to `reporting_webserver.py` (something like `C:\OceLogReader\reporting_webserver.py`)

Then run the service with

```
net start OceReportingServer
```

or you could use `services.msc` to start the OceReportingServer.


## Notes

**This is not an official tool from Canon or Oce and no support will be provided. Usage is subject to the included license.**

### General Notes

The PrismaSync controller only stores the last 99 days of jobs, so if you want a complete data history you must visit the web page at least once every 99 days (visiting the report webpage will download all the new data to the database). If you want to ensure that you don't miss any data you could setup a Windows scheduled task to run the `get_accounting_logs_to_db.py` on a reoccurring interval.

The database is dynamic and will update itself if the column data changes on the PrismaSync (eg if the CSV structure changes with new system releases) however if this happens the specific engine reporting will break as this is tried to the database structure at the time the report was designed.

As I have seen weird data in the .ACL file (this is the in-progress log file, the .CSV is not written until the end of the day) I have decided to always reprocess this file. This means that if you end up with the weird data, a web page refresh should fix it... (though I have not seen this when testing, so I don't know if it actually works)

The CSV files from the PrismaSync are only downloaded and processed once (unlike the .ACL file, which is downloaded and processed every time you refresh the report webpage)

Reporting is currently limited to the Oce Colorado 1640 and the CSV data that it provides in the 1.3.1.0 printer firmware version.

### Security

The usage data is stored un-encrypted in a SQLite database file. No job information is sent offsite. Periodically the scripts will check Bitbucket to see if you are running the latest version of the scripts (this happens the first time you open the reporting webpage and then every 31 days). The report pages require that the viewing browser can contact Googles servers to download the Source Sans Pro and Material Icons font files.

### Backup

The only important file is the `accountingdatabase.sql` file that is stored locally in the directory alongside the scripts. This should be backed up to ensure no data loss (as the PrismaSync controller only stores the last 99 days usage)

### Colorado 1640 Specific Notes

Prices shown don't include media costs.

#### Calculations

Total square meters is the sum of the `printedarea` (which in the CSV is in square mm) for all jobs in the database.

Total ink used is the sum of `inkcolorcyan+inkcolormagenta+inkcoloryellow+inkcolorblack ` (which in the CSV is in &mu;L) for all jobs in the database.

Total running time is the sum of `activetime` for all jobs in the database.

Average speed is total square meters / total running time.

Average cost is total ink * ink cost / total square meters.

## Todo

### General Todo

* Allow connection to multiple machines (The database classes have been designed so that you could have data being collected from multiple machines into the one database, however I have not written an interface to allow you specify multiple machines)
* Test on other PrismaSync controllers and write additional reports (eg varioPRINT i300)
* Colorwave Accounting (http://<ip>/Accounting/saveCounterHistory?filter=12weeks&format=csv)

### Colorado Todo

* Allow some way to include media pricing to calculate total job cost (ink plus media)
* Setup the print.css file so it prints nicely.
* Create a mobile css file so it displays nicely on iPhone
* Check display on different browsers (only currently checked on Chrome)
* Smarter logic allowing automatic determination of machine type (ie support dynamic ink pricing (eg if there is a column for white ink usage, use it))


## Version History

v 0.5 - Changed to allow white ink reports for Colorado M

v 0.4 - Added home icon to jobdetails pages

v 0.3 - Allow separate unit prices for CMYK inks.

v 0.2 - Added job details report to Colorado reporting

v 0.1 - Initial Beta Release