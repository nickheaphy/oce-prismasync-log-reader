# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world


from flask import Flask
import config

app = Flask(__name__)

from app import routes

# Set the secret key
app.secret_key = 'notverysecretatall' + str(config.__version__)
