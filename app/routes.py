from flask import render_template
from app import app
from inc import DB_Reporting_ColoradoM
import config
from inc import versioncheck
from flask import session
import logging
from datetime import datetime
import re
from flask import request

@app.context_processor
def inject_now():
    return {'now': datetime.utcnow()}

@app.route('/')
@app.route('/index')
def index():
        # check the version
        if 'latestversion' not in session:
                session['latestversion'] = versioncheck.return_latest_online_version()
                logger = logging.getLogger('oce_accounting')
                logger.debug("Version check: current = {}, latest = {}".format(config.__version__,session['latestversion']))

        #session['latestversion'] = 11

        with DB_Reporting_ColoradoM.ColoradoMReporting(
                config.config["databasename"],
                config.config["machinename"],
                (config.config["c_costperml"],config.config["m_costperml"],config.config["y_costperml"],config.config["k_costperml"],config.config["w_costperml"])
                ) as db:

                # attempt to pull the data from the machine
                db.getLogs(config.config["baseurl"],config.config["machinename"])

                # pull the data for display
                sqm, mls, runtimehours, spend = db.return5totalinkusage()
                inkcompbymedia = db.returninkconsumptionbymedia()
                inkcompbymode = db.returninkconsumptionbymode()
                lastjobs = db.returnlastxjobs(20)
                return render_template('inkusage.jinja',
                        sqm=sqm, mls=mls, spend=spend, runtimehours=runtimehours,
                        inkcompbymedia=inkcompbymedia,
                        inkcompbymode=inkcompbymode,
                        lastjobs=lastjobs,
                        latestversion=session['latestversion'],
                        currentversion=config.__version__,
                        home_url = versioncheck.__home_url__
                        )

@app.route('/jobdetails',methods = ['GET'])
def jobdetails():
        jobid = request.args.get('jobid')
        documentid = request.args.get('documentid')

        with DB_Reporting_ColoradoM.ColoradoMReporting(
                config.config["databasename"],
                config.config["machinename"],
                (config.config["c_costperml"],config.config["m_costperml"],config.config["y_costperml"],config.config["k_costperml"],config.config["w_costperml"])
                ) as db:
                jobdetails = db.returnjobdetails(jobid,documentid)
                return render_template('jobdetails.jinja',
                        alljobdetails=jobdetails,
                        inkcosts=(config.config["c_costperml"],config.config["m_costperml"],config.config["y_costperml"],config.config["k_costperml"],config.config["w_costperml"])
                )
