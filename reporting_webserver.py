# Start the flask app

from app import app
import logging
from config import config


# create logger
logger = logging.getLogger('oce_accounting')
logger.setLevel(config["loglevel"])
# create file handler which logs even debug messages
fh = logging.FileHandler('oce_accounting.log')
fh.setLevel(config["loglevel"])
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(config["loglevel"])
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
#ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

logger.debug("Starting Webserver")

if __name__ == "__main__":
    if config["loglevel"] == logging.DEBUG:
        app.run(host='0.0.0.0', port=config["webserverport"],debug=True)
    else:
        app.run(host='0.0.0.0', port=config["webserverport"])
    