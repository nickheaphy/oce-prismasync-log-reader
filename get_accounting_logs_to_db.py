# This is just a python script that pulls the data from the Oce into the database

from inc import parselogfile
from inc import DB_Class
import logging
from config import config

# baseurl = "http://10.10.0.106/accounting"
# machinename = "colorado"
# loglevel = logging.DEBUG


def main():

    with DB_Class.DB(config["databasename"]) as database:

        database.getLogs(config["baseurl"],config["machinename"])


if __name__ == "__main__":

    # create logger
    logger = logging.getLogger('oce_accounting')
    logger.setLevel(config["loglevel"])
    # create file handler which logs even debug messages
    fh = logging.FileHandler('oce_accounting.log')
    fh.setLevel(config["loglevel"])
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(config["loglevel"])
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    #ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    main()