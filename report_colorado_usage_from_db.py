# Report on the data from the database
# This just prints the data to screen

import inc.DB_Class as DB_Class
import logging
import inc.DB_Reporting_Colorado as DB_Reporting_Colorado
import datetime
import config

def floatconv(value):
    try:
        float(value)
        return float(value)
    except ValueError:
        return 0.0


def main():

    with DB_Reporting_Colorado.ColoradoReporting(
        config.config["databasename"],
        config.config["machinename"],
        (config.config["c_costperml"],config.config["m_costperml"],config.config["y_costperml"],config.config["k_costperml"])
        ) as database:

        print("\nInk Consumption Total")
        print("-----------------------")
        sqm, inkml, duration, spend = database.return4totalinkusage()
        print("{:.1f}sqm {:.1f}ml : {:.1f}ml/sqm".format(sqm,inkml,inkml/sqm))

        print("\nInk Consumption by Media")
        print("--------------------------")
        result = database.returninkconsumptionbymedia()
        for row in result:
            sqm = database.divideby(row["area"],1000000)
            inkml = database.divideby(row["ink"],1000)
            inkpersm = database.divideby(inkml,sqm)
            if isinstance(sqm, float) and isinstance(sqm, float) and isinstance(inkpersm, float):
                print("{:<53} {:.1f}sqm {:.1f}ml : {:.3f}ml/sqm"
                .format(row['mediatype'],sqm,inkml,inkpersm))
            else:
                print("{:<53} Unknownsqm Unknownml : Unknownml/sqm"
                .format(row['mediatype']))
        
        print("\nInk Consumption by PrintMode")
        print("------------------------------")
        result = database.returninkconsumptionbymode()
        for row in result:
            sqm = database.divideby(row["area"],1000000)
            inkml = database.divideby(row["ink"],1000)
            inkpersm = database.divideby(inkml,sqm)
            if isinstance(sqm, float) and isinstance(sqm, float) and isinstance(inkpersm, float):
                print("{:<15} {:.1f}sqm {:.1f}ml : {:.3f}ml/sqm"
                .format(row['printmode'],sqm,inkml,database.divideby(inkml,sqm,0)))
            else:
                print("{:<15} Unknownsqm Unknownml : Unknownml/sqm"
                .format(row['printmode']))

        print("\nInk Consumption by Job (Last 20 jobs)")
        print("------------------------------")
        result = database.returnlastxjobs(20)
        for row in result:
            sqm = database.divideby(row["area"],1000000)
            inkml = database.divideby(row["ink"],1000)
            inkpersm = database.divideby(inkml,sqm)
            cost = row["inkprice"]
            if isinstance(sqm, float) and isinstance(sqm, float) and isinstance(inkpersm, float):
                print("{:<15} on {} {:.1f}sqm {:.1f}ml : {:.3f}ml/sqm : ${:.2f}"
                .format(row['jobname'],row['mediatype'],sqm,inkml,database.divideby(inkml,sqm,0),cost))
            else:
                print("{:<15} on {} Unknownsqm Unknownml : Unknownml/sqm"
                .format(row['jobname'],row['mediatype']))


if __name__ == "__main__":

    # create logger
    logger = logging.getLogger('oce_accounting')
    logger.setLevel(config.config["loglevel"])
    # create file handler which logs even debug messages
    fh = logging.FileHandler('oce_accounting.log')
    fh.setLevel(config.config["loglevel"])
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(config.config["loglevel"])
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    #ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    main()