import logging

# This holds the configuration data. The baseurl and costperml should be changed
# to appropriate values for your site.

__home_url__ = "https://bitbucket.org/nickheaphy/oce-prismasync-log-reader"
__version__ = 0.5

# Here is a sample config for a Colorado M series with White Ink
config = {
    "machinename" : "coloradoM",                  # this is used to name the table in the database
    "model"       : "M",                         # this is the model. Currently supported 1640 or M.
    "baseurl" : "http://localhost:9000", # this is the URL for the accounting logs on the PrismaSync
    "c_costperml" : 499/2000,                    # ink price in $ per ml (Cyan)
    "m_costperml" : 499/2000,                    # ink price in $ per ml (Magenta)
    "y_costperml" : 499/2000,                    # ink price in $ per ml (Yellow)
    "k_costperml" : 499/2000,                    # ink price in $ per ml (Black)
    "w_costperml" : 499/2000,                    # ink price in $ per ml (White - for the M series)
    "databasename" : "accountingdatabaseM.sqlite3",   # the name of the database file
    #"loglevel" : logging.DEBUG,                 # the logging level
    "loglevel" : logging.WARN,                   # the logging level (logging.WARN is less chatty)
    "webserverport" : 8080                       # the port the web server is running on
}

# # Here is a sample config for a Colorado
# config = {
#     "machinename" : "colorado",                  # this is used to name the table in the database
#     "baseurl" : "http://10.10.0.100/accounting", # this is the URL for the accounting logs on the PrismaSync
#     "c_costperml" : 499/2000,                    # ink price in $ per ml (Cyan)
#     "m_costperml" : 499/2000,                    # ink price in $ per ml (Magenta)
#     "y_costperml" : 499/2000,                    # ink price in $ per ml (Yellow)
#     "k_costperml" : 499/2000,                    # ink price in $ per ml (Black)
#     "databasename" : "accountingdatabase.sql",   # the name of the database file
#     #"loglevel" : logging.DEBUG,                 # the logging level
#     "loglevel" : logging.WARN,                   # the logging level (logging.WARN is less chatty)
#     "webserverport" : 8080                       # the port the web server is running on
# }

# Here is a sample config for a varioPRINT iX
# config = {
#     "machinename" : "ix3200",                  # this is used to name the table in the database
#     "baseurl" : "http://172.17.4.10/accounting", # this is the URL for the accounting logs on the PrismaSync
#     "low_a4" : 499/2000,                    # 
#     "low_a3" : 499/2000,                    # 
#     "med_a4" : 499/2000,                    # 
#     "med_a3" : 499/2000,                    # 
#     "high_a4" : 499/2000,                    # ink price in $ per ml (Yellow)
#     "high_a3" : 499/2000,                    # ink price in $ per ml (Yellow)
#     "mono_a4" : 1,
#     "mono_a3" : 1,
#     "databasename" : "accountingdatabase.sql",   # the name of the database file
#     #"loglevel" : logging.DEBUG,                 # the logging level
#     "loglevel" : logging.WARN,                   # the logging level (logging.WARN is less chatty)
#     "webserverport" : 8080                       # the port the web server is running on
# }